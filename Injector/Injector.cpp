// Injector.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <cstring>

#include <easyhook.h>

int main()
{

	DWORD processId;
	std::wcout << "Enter the target process ID: ";
	std::cin >> processId;

	//TODO: inserire qui il path della dll da injectare.
	WCHAR* dllToInject = (WCHAR*)L".\\spawn.dll";
	wprintf(L"Attempting to inject %s\n\n", dllToInject);

	NTSTATUS nt = RhInjectLibrary(
		processId,
		0,
		EASYHOOK_INJECT_DEFAULT,
		dllToInject,
		NULL,
		NULL,
		0
	);

	if (nt != 0) {
		printf("RhInjectLibrary falied with error code = %d\n", nt);
		PWCHAR err = RtlGetLastErrorString();
		std::wcout << err << "\n";
	}
	else {
		std::wcout << L"Library injected succesfully.\n";
	}

	std::wcout << "Press Enter to exit";
	std::wstring input;
	std::getline(std::wcin, input);
	std::getline(std::wcin, input);

    return 0;
}

