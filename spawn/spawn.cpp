// spawn.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include <string>
#include <iostream>
#include <Windows.h>
#include <Hidsdi.h>
#include <fstream>

#include <easyhook.h>

std::string title = "Info";

LPCSTR lpText = title.c_str();

/*void myHidD_SetFeature(HANDLE HidDeviceObject, PVOID  ReportBuffer, ULONG  ReportBufferLength) {
	std::ofstream myfile;
	myfile.open("report_buffer.log");
	myfile << *((char*)ReportBuffer) << "\nsize: " << ReportBufferLength;
	myfile.close();
}*/

HANDLE myCreateFileA(
	LPCSTR                lpFileName,
	DWORD                 dwDesiredAccess,
	DWORD                 dwShareMode,
	LPSECURITY_ATTRIBUTES lpSecurityAttributes,
	DWORD                 dwCreationDisposition,
	DWORD                 dwFlagsAndAttributes,
	HANDLE                hTemplateFile
) {
	HANDLE myLogFile;
	std::string nomeFile = "logfile_test.txt";

	myLogFile = CreateFileA(nomeFile.c_str(), 0x0C0000000, 3, 0, 3, 0, 0);

	std::string lpText;
	std::string lpCaption;

	if (myLogFile == INVALID_HANDLE_VALUE)
	{
		lpText = "Unable to open file.";
		lpCaption = "Error";
		MessageBoxA(NULL, lpText.c_str(), lpCaption.c_str(), MB_OK);
		return INVALID_HANDLE_VALUE;
	}

	lpText = "File created";
	lpCaption = "Info";
	MessageBoxA(NULL, lpText.c_str(), lpCaption.c_str(), MB_OK);

	return myLogFile;
}

extern "C" void __declspec(dllexport) __stdcall NativeInjectionEntryPoint(REMOTE_ENTRY_INFO* inRemoteInfo);

void __stdcall NativeInjectionEntryPoint(REMOTE_ENTRY_INFO* inRemoteInfo) {
	
	HOOK_TRACE_INFO hHook = { NULL };
	std::cout << "Win32 HidD_GetHidGuid found at address: " << GetProcAddress(GetModuleHandle(TEXT("hid")), "HidD_GetHidGuid") << "\n";

	/*NTSTATUS result = LhInstallHook(
		GetProcAddress(GetModuleHandle(TEXT("hid")), "HidD_GetHidGuid"),
		myHidD_SetFeature,
		NULL,
		&hHook
	);*/

	NTSTATUS result = LhInstallHook(
		GetProcAddress(GetModuleHandle(TEXT("kernel32")), "CreateFileA"),
		myCreateFileA,
		NULL,
		&hHook
	);

	if (FAILED(result)) {
		std::wstring s(RtlGetLastErrorString());
		std::wcout << "Failed to install hook: ";
		std::wcout << s;
	}
	else {
		std::cout << "Hook 'myBeepHook installed successfully.";
	}

	// If the threadId in the ACL is set to 0,
	// then internally EasyHook uses GetCurrentThreadId()
	ULONG ACLEntries[1] = { 0 };

	// Disable the hook for the provided threadIds, enable for all others
	LhSetExclusiveACL(ACLEntries, 1, &hHook);

	return;

}

